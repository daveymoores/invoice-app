/**main app js**/


var app = angular.module('invoice2', ['finance', 'ngRoute']); //has dependency ng-route

//routing
app.config(function($routeProvider){
  $routeProvider.when("/",
    {
      templateUrl: "view_login/login.html",
      controller: "InvoiceController"
    })
  	.when("/client",
    {
      templateUrl: "view1/client_view.html",
      controller: "InvoiceController"
    }) 
  	.when("/projects",
    {
      templateUrl: "view2/projects_view.html",
      controller: "InvoiceController"
    }) 
    .when("/invoice",
    {
      templateUrl: "view3/invoice_gen.html",
      controller: "InvoiceController"
    });
});


//invoice controller - handles projects
app.controller('InvoiceController', ['currencyConverter', '$scope', function(currencyConverter, $scope) {
	this.desc= "";
	this.qty = 0;
	this.cost = 0;
	this.inCurr = 'EUR';
	this.currencies = currencyConverter.currencies; //asign currencies from finance.js

	//var projects = $scope.projects; //= projectStorage.get();

	$scope.newProj = '';
	$scope.projects  = []; //initilise the project array

	$scope.removeProj = function (project) {
		$scope.projects.splice($scope.projects.indexOf(project), 1);
	}

	$scope.delete = function ( idx ) {
	  var project_removed = $scope.projects[idx];

	   $scope.projects.splice(idx, 1);
	};

	$scope.generateInvoice = function() {
		window.location = '#/invoice';
	}


	// this.total = function total(outCurr) {

	//   var el = currencyConverter.convert(this.qty * this.cost, this.inCurr, outCurr);
	//   console.log(el);
	//   return el;

	// };

	this.total = function total(outCurr) {
	  var el = currencyConverter.convert(this.qty * this.cost, this.inCurr, outCurr);
	  console.log(el);
	  return el;
	};

	$scope.reset = function() {
		$scope.invoice.cost = 0;
		$scope.invoice.qty = 0;
		$scope.invoice.desc = "";
		$scope.invoice.inCurr = "EUR";
    }

	// $scope.editProject = function (invoice) {
	// 	$scope.editedProject = invoice;
	// 	// Clone the original todo to restore it on demand.
	// 	$scope.originalProject = angular.extend({}, invoice);
	// };

	// $scope.doneEditing = function (invoice) {
	// 	$scope.editedProject = null;
	// 	invoice.desc = invoice.desc.trim();

	// 	if (!invoice.desc) {
	// 		$scope.removeProject(invoice);
	// 	}
	// };

	//add a project
	$scope.addProj = function () {
		
		var newProj = $scope.invoice.desc.trim(); //take out white space
		if (!newProj.length) {
			return; //is there a project? if not then return
		}

    	$scope.projects.push({desc:$scope.invoice.desc, qty:$scope.invoice.qty, cost:$scope.invoice.cost, inCurr:$scope.invoice.inCurr, total:$scope.invoice.total});

		console.log(total()); //trying to get total output
	};


}]);



//controller for client behaviours
app.controller('clientController', function($scope) {

	this.company= "";
	this.first = "";
	this.last = "";
	this.position = "";
	this.email = "";
	this.phone = "";
	this.notes = "";

	$scope.newCli = '';
	$scope.clients  = [];



	//client behaviours

	$scope.clientAdd = function () {

		var newCli = $scope.client.company.trim(); //take out white space
		if (!newCli.length) {

			return; //is there a project? if not then return
		} else {
			window.location = '#/projects'; //if there, assume completed and move to project page
		}

    	$scope.clients.push({company:$scope.client.company, first:$scope.client.first, last:$scope.client.last, pos:$scope.client.position, email:$scope.client.email, phone:$scope.client.phone, notes:$scope.client.notes});

    	console.log($scope.clients);
	}
});


//controller for client behaviours
app.controller('userController', function($scope) {

	this.email= "";
	this.first = "";
	this.last = "";
	this.value1 = "";
	this.value2 = "";

	$scope.newUser = '';
	$scope.users  = [];

	//client behaviours

	$scope.userAdd = function () {
		var newUser = $scope.user.email.trim(); //take out white space
		if (!newUser.length) {

			return; //is there a project? if not then return

		} else {
			
			window.location = '#/client'; //if there, assume completed and move to project page
		}

    	$scope.users.push({email:$scope.user.email, first:$scope.user.first, last:$scope.user.last, password:$scope.user.value1});

    	console.log($scope.users);
	}

});


//password validation
app.directive('equals', function() {
  return {
    restrict: 'A', // only activate on element attribute
    require: '?ngModel', // get a hold of NgModelController
    link: function(scope, elem, attrs, ngModel) {
      if(!ngModel) return; // do nothing if no ng-model

      // watch own value and re-validate on change
      scope.$watch(attrs.ngModel, function() {
        validate();
      });

      // observe the other value and re-validate on change
      attrs.$observe('equals', function (val) {
        validate();
      });

      var validate = function() {
        // values
        var val1 = ngModel.$viewValue;
        var val2 = attrs.equals;

        // set validity
        ngModel.$setValidity('equals', ! val1 || ! val2 || val1 === val2);
      };
    }
  }
});


//when user is hovering over project entry
app.directive("projectentry", function(){
	return {

		restrict: "A",
		$scope: {},
		templateUrl: "templates/projectentry.html",
		link: function(scope, element, attrs) {
			element.bind("mouseenter", function(){
				scope.$apply(attrs.projectentry);	
			});
		
		}
	}
});

//adding projects
app.directive("projectadd", function(){
	return {

		restrict: "A",
		$scope: {},
		link: function(scope, element, attrs) {
			element.on("click", function(){
				scope.$apply(attrs.projectadd);
			});
		
		}
	}
});

//deleting projects
app.directive("projectremove", function(){
	return {

		restrict: "A",
		link: function(scope, element, attrs) {
			element.on("click", function(){
				scope.$apply(attrs.projectremove);
			});
		
		}
	}
});

app.directive("clientadd", function(){
	return {

		restrict: "A",
		link: function(scope, element, attrs) {
			element.on("click", function(){

				scope.$apply(attrs.clientadd);
			});
		
		}
	}
});

app.directive("useradd", function(){
	return {

		restrict: "A",
		link: function(scope, element, attrs) {
			element.on("click", function(){
				scope.$apply(attrs.useradd);
			});
		
		}
	}
});

//paying invoices
app.directive("invoicegen", function(){
	return {

		restrict: "A",
		link: function(scope, element, attrs) {
			element.on("click", function(){
				scope.$apply(attrs.invoicegen);
			});
		
		}
	}
});