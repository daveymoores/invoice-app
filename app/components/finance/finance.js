/**finance js**/

angular.module('finance', [])
  .factory('currencyConverter', function() {
    var currencies = ['USD', 'EUR', 'CNY'];
    var usdToForeignRates = {
      USD: 1,
      EUR: 0.74,
      CNY: 6.09
    };

    //not using currencies at the moment...
    // var convert = function (amount, inCurr, outCurr) {
    //   return amount * usdToForeignRates[outCurr] / usdToForeignRates[inCurr];
    // };

    var convert = function (amount, inCurr) {
      return amount * usdToForeignRates[inCurr];
    };

    return {
      currencies: currencies,
      convert: convert
    };
  });